﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Bloque
    {
        #region Variables
        private float x;
        private float y;
        private int vidas;
        private bool esIndestructible;
        #endregion
        #region Propiedades
        public float X
        {
            get { return x; }
            set { x = value; }
        }
        public float Y
        {
            get { return y; }
            set { y = value; }
        }
        public int Vidas
        {
            get { return vidas; }
            set { vidas = value; }
        }
        public bool EsIndestructible
        {
            get { return esIndestructible; }
            set { esIndestructible = value; }
        }
        #endregion
    }
}

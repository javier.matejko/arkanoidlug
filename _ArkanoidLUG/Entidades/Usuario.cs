﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Usuario
    {
        #region Variables
        private int id;
        private int id_Estadisticas;
        private string nombre;
        private string contraseña;
        private DateTime fecha_Alta;
        private DateTime fecha_Baja;
        private int activo;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Id_Estadisticas
        {
            get { return id_Estadisticas; }
            set { id_Estadisticas = value; }
        }
        public string Nombre_Usuario
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }
        public DateTime Fecha_Alta
        {
            get { return fecha_Alta; }
            set { fecha_Alta = value; }
        }
        public DateTime Fecha_Baja
        {
            get { return fecha_Baja; }
            set { fecha_Baja = value; }
        }
        public int Activo
        {
            get { return activo; }
            set { activo = value; }
        }
        #endregion        
    }
}

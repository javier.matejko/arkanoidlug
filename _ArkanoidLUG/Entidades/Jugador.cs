﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Jugador
    {
        #region Variables
        private int x;
        private int y;
        private int vidas;
        private int velocidad;
        private int puntaje;
        #endregion
        #region Propiedades
        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public int Vidas
        {
            get { return vidas; }
            set { vidas = value; }
        }
        public int Velocidad
        {
            get { return velocidad; }
            set { velocidad = value; }
        }
        public int Puntaje
        {
            get { return puntaje; }
            set { puntaje = value; }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Estadisticas
    {
        #region Variables
        private int id;
        private int id_Usuario;
        private Int64 tiempo_Jugado;
        private int partidas_Ganadas;
        private int partidas_Perdidas;
        private int promedio_Partidas;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Id_Usuario
        {
            get { return id_Usuario; }
            set { id_Usuario = value; }
        }
        public Int64 Tiempo_Jugado
        {
            get { return tiempo_Jugado; }
            set { tiempo_Jugado = value; }
        }
        public int Partidas_Ganadas
        {
            get { return partidas_Ganadas; }
            set { partidas_Ganadas = value; }
        }
        public int Partidas_Perdidas
        {
            get { return partidas_Perdidas; }
            set { partidas_Perdidas = value; }
        }
        private int Promedio_Partidas
        {
            get { return promedio_Partidas; }
            set { promedio_Partidas = value; }
        }
        #endregion
    }
}

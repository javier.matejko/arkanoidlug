﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArkanoidLUG
{
    public partial class FormMenuPrincipal : Form
    {
        Motor.Usuario gestor = new Motor.Usuario();
        Entidades.Usuario usuario;
        FormJuego frmJuego;
        public FormMenuPrincipal()
        {
            InitializeComponent();
        }

        private void registrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormRegistro frmRegistro = new FormRegistro();
            frmRegistro.ShowDialog();
        }

        private void ingresarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormLogin frmLogin = new FormLogin();
            frmLogin.ShowDialog();

            if(frmLogin.usuario != null)
            {
                usuario = frmLogin.usuario;
                frmJuego = new FormJuego(usuario);
                frmJuego.TopLevel = false;
                panelJuego.Controls.Add(frmJuego);
                frmJuego.Show();
                pausarToolStripMenuItem.Visible = true;                
            }            
        }

        private void FormJuego_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(usuario != null)
            {
                gestor.CerrarSesion(usuario);
                gestor.RegistrarCierreSesionPartida(usuario);
            }
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (usuario != null)
            {
                gestor.CerrarSesion(usuario);
                
                
                if(frmJuego != null)
                {
                    frmJuego.Close();
                    gestor.RegistrarCierreSesionPartida(usuario);
                    pausarToolStripMenuItem.Visible = false;
                }
                usuario = null;

                MessageBox.Show("Se ha cerrado correctamente la sesión!");
            }
            else
            {
                MessageBox.Show("Ningun usuario ha ingresado");
            }
        }

        private void pausarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(frmJuego != null)
            {
                frmJuego.Pausar();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Motor;

namespace ArkanoidLUG
{
    public partial class FormJuego : Form
    {
        Motor.Usuario gestor = new Motor.Usuario();
        Motor.Estadisticas gestorEstadisticas = new Motor.Estadisticas();
        public Entidades.Usuario usuario = new Entidades.Usuario();

        bool irIzquierda;
        bool irDerecha;
        bool gano;
        int pelotaX;
        int pelotaY;
        Juego juego = new Juego();
        Random rnd = new Random();
        List<PictureBox> bloques = new List<PictureBox>();
        Int64 tiempoJugado;
        int milesima;
        public FormJuego(Entidades.Usuario _usuario)
        {
            usuario = _usuario;
            InitializeComponent();
        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            juego.Inicializar();
            timerJuego.Start();
            timer.Start();
            lblPuntaje.Text = "Puntaje: " + juego.Jugador.Puntaje;
            pelotaX = 5;
            pelotaY = 5;

            foreach(Control c in this.Controls)
            {
                if(c is PictureBox && (string)c.Tag == "bloques")
                {
                    bloques.Add((PictureBox)c);
                }
            }

            gestor.RegistrarInicioSesionPartida(usuario);

            panelMenu.Visible = false;
            panelMenu.Enabled = false;
            panelMenu.SendToBack();
        }

        private void btnPausar_Click(object sender, EventArgs e)
        {
            panelPausa.Visible = true;
            panelPausa.Enabled = true;
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            panelPausa.Visible = false;
            panelPausa.Enabled = false;
            this.timerJuego.Start();
            this.timer.Start();
        }

        public void Pausar()
        {
            panelPausa.Visible = true;
            panelPausa.Enabled = true;
            this.timerJuego.Stop();
            this.timer.Stop();
        }

        private void timerJuego_Tick(object sender, EventArgs e)
        {
            if(irIzquierda && pboxJugador.Left > 0)
            {
                pboxJugador.Left -= juego.Jugador.Velocidad;
            }
            if (irDerecha && pboxJugador.Left < this.Width - pboxJugador.Width)
            {
                pboxJugador.Left += juego.Jugador.Velocidad;
            }

            pboxPelota.Left += pelotaX;
            pboxPelota.Top += pelotaY;

            if(pboxPelota.Left < 0 || pboxPelota.Left > this.Width - pboxPelota.Width)
            {
                pelotaX = -pelotaX;
            }
            if(pboxPelota.Top < 0)
            {
                pelotaY = -pelotaY;
            }
            if(pboxPelota.Bounds.IntersectsWith(pboxJugador.Bounds))
            {
                pelotaY = rnd.Next(5, 12) * -1;
                if(pelotaX < 0)
                {
                    pelotaX = rnd.Next(5, 12) * -1;
                }
                else
                {
                    pelotaX = rnd.Next(5, 12);
                }
            }

            foreach (Control c in this.Controls)
            {
                if (c is PictureBox && (string)c.Tag == "bloques")
                {
                    if(pboxPelota.Bounds.IntersectsWith(c.Bounds))
                    {
                        juego.Jugador.Puntaje += 150;
                        lblPuntaje.Text = "Puntaje: " + juego.Jugador.Puntaje;
                        pelotaY = -pelotaY;

                        this.Controls.Remove(c);
                        this.bloques.Remove((PictureBox)c);
                    }
                }
            }

            if(pboxPelota.Top > this.Height)
            {
                gano = false;
                GameOver("Perdiste!");
            }

            if(this.bloques.Count <= 0)
            {
                gano = true;
                GameOver("Ganaste!");
            }

        }

        private void FormJuego_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.A)
            {
                irIzquierda = true;                
            }
            if(e.KeyCode == Keys.D)
            {
                irDerecha = true;
            }
        }

        private void FormJuego_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A)
            {
                irIzquierda = false;
            }
            if (e.KeyCode == Keys.D)
            {
                irDerecha = false;
            }
        }

        //private List<Bloque> ObtenerBloques()
        //{
        //    List<Bloque> bloques = new List<Bloque>();
        //    foreach(Control c in this.Controls)
        //    {
        //        if(c is PictureBox && (string)c.Tag == "bloques")
        //        {
        //            c.BackColor = Color.FromArgb(50, 50, 50);
        //        }
        //    }

        //    return bloques;
        //}

        private void GameOver(string mensaje)
        {
            this.timerJuego.Stop();
            this.timer.Stop();

            lblPuntaje.Text = "Puntaje: " + juego.Jugador.Puntaje + " " + mensaje;

            gestor.RegistrarEstadisticas(usuario, tiempoJugado, gano);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            milesima++;
            if(milesima % 60 == 0)
            {
                tiempoJugado++;
            }
            
            lblTiempo.Text = "Tiempo: " + tiempoJugado;
        }

        private void btnPuntuacion_Click(object sender, EventArgs e)
        {
            DataTable tabla = gestorEstadisticas.LeerEstadisticas(usuario);

            foreach (DataRow registro in tabla.Rows)
            {
                lblTiempoJugado.Text = "Tiempo jugado: " + registro[2].ToString();
                lblPromedio.Text = "Promedio de Partidas Ganadas: " + registro[5].ToString();
            }
        }
    }
}

﻿namespace ArkanoidLUG
{
    partial class FormJuego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnJugar = new System.Windows.Forms.Button();
            this.btnPuntuacion = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.lblPromedio = new System.Windows.Forms.Label();
            this.lblTiempoJugado = new System.Windows.Forms.Label();
            this.panelPausa = new System.Windows.Forms.Panel();
            this.btnContinuar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pboxJugador = new System.Windows.Forms.PictureBox();
            this.pboxPelota = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.lblPuntaje = new System.Windows.Forms.Label();
            this.timerJuego = new System.Windows.Forms.Timer(this.components);
            this.lblTiempo = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panelMenu.SuspendLayout();
            this.panelPausa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxJugador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxPelota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(291, 16);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(228, 46);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "ARKANOID";
            // 
            // btnJugar
            // 
            this.btnJugar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnJugar.Location = new System.Drawing.Point(283, 111);
            this.btnJugar.Name = "btnJugar";
            this.btnJugar.Size = new System.Drawing.Size(234, 86);
            this.btnJugar.TabIndex = 1;
            this.btnJugar.Text = "Jugar";
            this.btnJugar.UseVisualStyleBackColor = true;
            this.btnJugar.Click += new System.EventHandler(this.btnJugar_Click);
            // 
            // btnPuntuacion
            // 
            this.btnPuntuacion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPuntuacion.Location = new System.Drawing.Point(283, 263);
            this.btnPuntuacion.Name = "btnPuntuacion";
            this.btnPuntuacion.Size = new System.Drawing.Size(234, 86);
            this.btnPuntuacion.TabIndex = 2;
            this.btnPuntuacion.Text = "Puntuacion";
            this.btnPuntuacion.UseVisualStyleBackColor = true;
            this.btnPuntuacion.Click += new System.EventHandler(this.btnPuntuacion_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panelMenu.Controls.Add(this.lblPromedio);
            this.panelMenu.Controls.Add(this.lblTiempoJugado);
            this.panelMenu.Controls.Add(this.lblTitulo);
            this.panelMenu.Controls.Add(this.btnPuntuacion);
            this.panelMenu.Controls.Add(this.btnJugar);
            this.panelMenu.Location = new System.Drawing.Point(64, 2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(810, 451);
            this.panelMenu.TabIndex = 3;
            // 
            // lblPromedio
            // 
            this.lblPromedio.AutoSize = true;
            this.lblPromedio.ForeColor = System.Drawing.Color.White;
            this.lblPromedio.Location = new System.Drawing.Point(614, 314);
            this.lblPromedio.Name = "lblPromedio";
            this.lblPromedio.Size = new System.Drawing.Size(169, 13);
            this.lblPromedio.TabIndex = 4;
            this.lblPromedio.Text = "Promedio de Victorias: 999999999";
            // 
            // lblTiempoJugado
            // 
            this.lblTiempoJugado.AutoSize = true;
            this.lblTiempoJugado.ForeColor = System.Drawing.Color.White;
            this.lblTiempoJugado.Location = new System.Drawing.Point(614, 290);
            this.lblTiempoJugado.Name = "lblTiempoJugado";
            this.lblTiempoJugado.Size = new System.Drawing.Size(149, 13);
            this.lblTiempoJugado.TabIndex = 3;
            this.lblTiempoJugado.Text = "Tiempo jugado: 99999999999";
            // 
            // panelPausa
            // 
            this.panelPausa.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panelPausa.Controls.Add(this.btnContinuar);
            this.panelPausa.Controls.Add(this.label1);
            this.panelPausa.Location = new System.Drawing.Point(182, 98);
            this.panelPausa.Name = "panelPausa";
            this.panelPausa.Size = new System.Drawing.Size(577, 233);
            this.panelPausa.TabIndex = 5;
            this.panelPausa.Visible = false;
            // 
            // btnContinuar
            // 
            this.btnContinuar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContinuar.Location = new System.Drawing.Point(209, 132);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(150, 50);
            this.btnContinuar.TabIndex = 5;
            this.btnContinuar.Text = "Continuar";
            this.btnContinuar.UseVisualStyleBackColor = true;
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(104, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(361, 76);
            this.label1.TabIndex = 0;
            this.label1.Text = "PAUSADO";
            // 
            // pboxJugador
            // 
            this.pboxJugador.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pboxJugador.BackColor = System.Drawing.Color.Black;
            this.pboxJugador.Location = new System.Drawing.Point(412, 413);
            this.pboxJugador.Name = "pboxJugador";
            this.pboxJugador.Size = new System.Drawing.Size(100, 25);
            this.pboxJugador.TabIndex = 6;
            this.pboxJugador.TabStop = false;
            // 
            // pboxPelota
            // 
            this.pboxPelota.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pboxPelota.BackColor = System.Drawing.Color.Yellow;
            this.pboxPelota.Location = new System.Drawing.Point(448, 353);
            this.pboxPelota.Name = "pboxPelota";
            this.pboxPelota.Size = new System.Drawing.Size(25, 25);
            this.pboxPelota.TabIndex = 7;
            this.pboxPelota.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(28, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 25);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "bloques";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(133, 48);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(75, 25);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "bloques";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Location = new System.Drawing.Point(233, 48);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(75, 25);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "bloques";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.Location = new System.Drawing.Point(332, 48);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(75, 25);
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "bloques";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox5.BackColor = System.Drawing.Color.White;
            this.pictureBox5.Location = new System.Drawing.Point(28, 90);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(75, 25);
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Tag = "bloques";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox6.BackColor = System.Drawing.Color.White;
            this.pictureBox6.Location = new System.Drawing.Point(133, 90);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(75, 25);
            this.pictureBox6.TabIndex = 13;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Tag = "bloques";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox7.BackColor = System.Drawing.Color.White;
            this.pictureBox7.Location = new System.Drawing.Point(28, 135);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(75, 25);
            this.pictureBox7.TabIndex = 14;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "bloques";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox8.BackColor = System.Drawing.Color.White;
            this.pictureBox8.Location = new System.Drawing.Point(133, 135);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(75, 25);
            this.pictureBox8.TabIndex = 15;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Tag = "bloques";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox9.BackColor = System.Drawing.Color.White;
            this.pictureBox9.Location = new System.Drawing.Point(233, 90);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(75, 25);
            this.pictureBox9.TabIndex = 16;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Tag = "bloques";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox10.BackColor = System.Drawing.Color.White;
            this.pictureBox10.Location = new System.Drawing.Point(233, 135);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(75, 25);
            this.pictureBox10.TabIndex = 17;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Tag = "bloques";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox11.BackColor = System.Drawing.Color.White;
            this.pictureBox11.Location = new System.Drawing.Point(332, 90);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(75, 25);
            this.pictureBox11.TabIndex = 18;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Tag = "bloques";
            // 
            // pictureBox12
            // 
            this.pictureBox12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox12.BackColor = System.Drawing.Color.White;
            this.pictureBox12.Location = new System.Drawing.Point(332, 135);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(75, 25);
            this.pictureBox12.TabIndex = 19;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Tag = "bloques";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox13.BackColor = System.Drawing.Color.White;
            this.pictureBox13.Location = new System.Drawing.Point(636, 135);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(75, 25);
            this.pictureBox13.TabIndex = 28;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Tag = "bloques";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox14.BackColor = System.Drawing.Color.White;
            this.pictureBox14.Location = new System.Drawing.Point(636, 90);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(75, 25);
            this.pictureBox14.TabIndex = 27;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Tag = "bloques";
            // 
            // pictureBox15
            // 
            this.pictureBox15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox15.BackColor = System.Drawing.Color.White;
            this.pictureBox15.Location = new System.Drawing.Point(537, 135);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(75, 25);
            this.pictureBox15.TabIndex = 26;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Tag = "bloques";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox16.BackColor = System.Drawing.Color.White;
            this.pictureBox16.Location = new System.Drawing.Point(537, 90);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(75, 25);
            this.pictureBox16.TabIndex = 25;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Tag = "bloques";
            // 
            // pictureBox17
            // 
            this.pictureBox17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox17.BackColor = System.Drawing.Color.White;
            this.pictureBox17.Location = new System.Drawing.Point(437, 135);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(75, 25);
            this.pictureBox17.TabIndex = 24;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Tag = "bloques";
            // 
            // pictureBox18
            // 
            this.pictureBox18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox18.BackColor = System.Drawing.Color.White;
            this.pictureBox18.Location = new System.Drawing.Point(437, 90);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(75, 25);
            this.pictureBox18.TabIndex = 23;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Tag = "bloques";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox19.BackColor = System.Drawing.Color.White;
            this.pictureBox19.Location = new System.Drawing.Point(636, 48);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(75, 25);
            this.pictureBox19.TabIndex = 22;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Tag = "bloques";
            // 
            // pictureBox20
            // 
            this.pictureBox20.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox20.BackColor = System.Drawing.Color.White;
            this.pictureBox20.Location = new System.Drawing.Point(537, 48);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(75, 25);
            this.pictureBox20.TabIndex = 21;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Tag = "bloques";
            // 
            // pictureBox21
            // 
            this.pictureBox21.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox21.BackColor = System.Drawing.Color.White;
            this.pictureBox21.Location = new System.Drawing.Point(437, 48);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(75, 25);
            this.pictureBox21.TabIndex = 20;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Tag = "bloques";
            // 
            // pictureBox22
            // 
            this.pictureBox22.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox22.BackColor = System.Drawing.Color.White;
            this.pictureBox22.Location = new System.Drawing.Point(845, 135);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(75, 25);
            this.pictureBox22.TabIndex = 34;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Tag = "bloques";
            // 
            // pictureBox23
            // 
            this.pictureBox23.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox23.BackColor = System.Drawing.Color.White;
            this.pictureBox23.Location = new System.Drawing.Point(845, 90);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(75, 25);
            this.pictureBox23.TabIndex = 33;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.Tag = "bloques";
            // 
            // pictureBox24
            // 
            this.pictureBox24.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox24.BackColor = System.Drawing.Color.White;
            this.pictureBox24.Location = new System.Drawing.Point(746, 135);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(75, 25);
            this.pictureBox24.TabIndex = 32;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.Tag = "bloques";
            // 
            // pictureBox25
            // 
            this.pictureBox25.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox25.BackColor = System.Drawing.Color.White;
            this.pictureBox25.Location = new System.Drawing.Point(746, 90);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(75, 25);
            this.pictureBox25.TabIndex = 31;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.Tag = "bloques";
            // 
            // pictureBox26
            // 
            this.pictureBox26.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox26.BackColor = System.Drawing.Color.White;
            this.pictureBox26.Location = new System.Drawing.Point(845, 48);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(75, 25);
            this.pictureBox26.TabIndex = 30;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.Tag = "bloques";
            // 
            // pictureBox27
            // 
            this.pictureBox27.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox27.BackColor = System.Drawing.Color.White;
            this.pictureBox27.Location = new System.Drawing.Point(746, 48);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(75, 25);
            this.pictureBox27.TabIndex = 29;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.Tag = "bloques";
            // 
            // lblPuntaje
            // 
            this.lblPuntaje.AutoSize = true;
            this.lblPuntaje.BackColor = System.Drawing.Color.Transparent;
            this.lblPuntaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPuntaje.ForeColor = System.Drawing.Color.White;
            this.lblPuntaje.Location = new System.Drawing.Point(12, 9);
            this.lblPuntaje.Name = "lblPuntaje";
            this.lblPuntaje.Size = new System.Drawing.Size(134, 20);
            this.lblPuntaje.TabIndex = 35;
            this.lblPuntaje.Text = "Puntaje: 9999999";
            // 
            // timerJuego
            // 
            this.timerJuego.Interval = 20;
            this.timerJuego.Tick += new System.EventHandler(this.timerJuego_Tick);
            // 
            // lblTiempo
            // 
            this.lblTiempo.AutoSize = true;
            this.lblTiempo.BackColor = System.Drawing.Color.Transparent;
            this.lblTiempo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTiempo.ForeColor = System.Drawing.Color.White;
            this.lblTiempo.Location = new System.Drawing.Point(178, 9);
            this.lblTiempo.Name = "lblTiempo";
            this.lblTiempo.Size = new System.Drawing.Size(65, 20);
            this.lblTiempo.TabIndex = 36;
            this.lblTiempo.Text = "Tiempo:";
            // 
            // timer
            // 
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // FormJuego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(941, 450);
            this.Controls.Add(this.lblTiempo);
            this.Controls.Add(this.panelPausa);
            this.Controls.Add(this.pboxJugador);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.pboxPelota);
            this.Controls.Add(this.lblPuntaje);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox24);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox21);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormJuego";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormJuego";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormJuego_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormJuego_KeyUp);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.panelPausa.ResumeLayout(false);
            this.panelPausa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxJugador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxPelota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnJugar;
        private System.Windows.Forms.Button btnPuntuacion;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Panel panelPausa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnContinuar;
        private System.Windows.Forms.PictureBox pboxPelota;
        private System.Windows.Forms.PictureBox pboxJugador;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblPuntaje;
        private System.Windows.Forms.Timer timerJuego;
        private System.Windows.Forms.Label lblTiempo;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lblPromedio;
        private System.Windows.Forms.Label lblTiempoJugado;
    }
}
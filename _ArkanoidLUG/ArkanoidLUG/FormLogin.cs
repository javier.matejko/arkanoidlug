﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArkanoidLUG
{
    public partial class FormLogin : Form
    {
        Motor.Usuario gestor = new Motor.Usuario();
        public Entidades.Usuario usuario = new Entidades.Usuario();
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            usuario.Nombre_Usuario = txtNombre_Usuario.Text;
            usuario.Contraseña = txtContraseña.Text;

            if(gestor.Ingresar(usuario) > 0)
            {
                DataTable tabla = gestor.LeerUsuario(usuario);

                foreach (DataRow registro in tabla.Rows)
                {
                    usuario.Id = int.Parse(registro[0].ToString());
                    usuario.Id_Estadisticas = int.Parse(registro[1].ToString());
                    usuario.Nombre_Usuario = registro[2].ToString();
                    usuario.Contraseña = registro[3].ToString();
                    usuario.Fecha_Alta = DateTime.Parse(registro[4].ToString());
                    //usuario.Fecha_Baja = null;
                    usuario.Activo = int.Parse(registro[6].ToString());
                }

                
                gestor.RegistroSesion(usuario);
                MessageBox.Show("Ingreso exitoso!");
                this.Close();
            }
            else
            {
                usuario = null;
                MessageBox.Show("Usuario/Contraseña incorrecto");
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            FormRegistro frmRegistro = new FormRegistro();
            frmRegistro.ShowDialog();
        }
    }
}

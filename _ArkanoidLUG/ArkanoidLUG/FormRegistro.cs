﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArkanoidLUG
{
    public partial class FormRegistro : Form
    {
        Motor.Usuario gestor = new Motor.Usuario();
        public FormRegistro()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Entidades.Usuario usuario = new Entidades.Usuario();
            usuario.Nombre_Usuario = txtNombre_Usuario.Text;
            if(txtContraseña.Text == txtContraseña_Reingrese.Text)
            {
                usuario.Contraseña = txtContraseña.Text;
            }
            else
            {
                MessageBox.Show("Contraseñas ingresadas no coinciden");
                return;
            }
            
            usuario.Fecha_Alta = DateTime.Now;

            gestor.Guardar(usuario);

            MessageBox.Show("Usuario registrado con éxito!");
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class MP_Estadisticas
    {
        private Acceso acceso = new Acceso();

        public DataTable LeerEstadisticas(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_usuario", usuario.Id));

            DataTable tabla = acceso.Leer("LeerEstadisticas", parametros);

            acceso.CerrarConexion();

            return tabla;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class MP_Usuario
    {
        private Acceso acceso = new Acceso();

        public void Insertar(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre_usuario", usuario.Nombre_Usuario));
            parametros.Add(acceso.CrearParametro("@contraseña", usuario.Contraseña));
            parametros.Add(acceso.CrearParametro("@fecha_alta", usuario.Fecha_Alta));

            acceso.Escribir("Usuario_Add", parametros);

            acceso.CerrarConexion();
        }
        public DataTable LeerUsuario(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre_usuario", usuario.Nombre_Usuario));

            DataTable tabla = acceso.Leer("LeerUsuarioCompleto", parametros);

            acceso.CerrarConexion();

            return tabla;
        }
        public int Leer(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre_usuario", usuario.Nombre_Usuario));
            parametros.Add(acceso.CrearParametro("@contraseña", usuario.Contraseña));

            DataTable tabla = acceso.Leer("LeerUsuario", parametros);

            acceso.CerrarConexion();

            return tabla.Rows.Count;
        }

        public void RegistrarSesion(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre_usuario", usuario.Nombre_Usuario));

            acceso.Escribir("Registro_Usuario_Log_Add", parametros);

            acceso.CerrarConexion();
        }

        public void CerrarSesion(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre_usuario", usuario.Nombre_Usuario));

            acceso.Escribir("Registro_Usuario_Log_Sesion_Mod", parametros);

            acceso.CerrarConexion();
        }

        public void RegistrarEstadisticas(Entidades.Usuario usuario, Int64 tiempoJugado, bool gano)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_usuario", usuario.Id));
            parametros.Add(acceso.CrearParametro("@tiempo_jugado", tiempoJugado));

            if(gano)
            {
                parametros.Add(acceso.CrearParametro("@partidas_ganadas", 1));
                parametros.Add(acceso.CrearParametro("@partidas_perdidas", 0));
            }
            else
            {
                parametros.Add(acceso.CrearParametro("@partidas_ganadas", 0));
                parametros.Add(acceso.CrearParametro("@partidas_perdidas", 1));
            }

            acceso.Escribir("Estadisticas_Mod", parametros);

        }

        public void RegistrarInicioSesionPartida(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre_usuario", usuario.Nombre_Usuario));
            parametros.Add(acceso.CrearParametro("@fecha_inicio", DateTime.Now));
            //parametros.Add(acceso.CrearParametro("@fecha_cierre", null));

            acceso.Escribir("Registro_Usuario_Log_Partida_Mod", parametros);

            acceso.CerrarConexion();
        }

        public void RegistrarCierreSesionPartida(Entidades.Usuario usuario)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre_usuario", usuario.Nombre_Usuario));
            //parametros.Add(acceso.CrearParametro("@fecha_inicio", null));
            parametros.Add(acceso.CrearParametro("@fecha_cierre", DateTime.Now));

            acceso.Escribir("Registro_Usuario_Log_Partida_Mod", parametros);

            acceso.CerrarConexion();
        }
    }
}

USE [master]
GO
/****** Object:  Database [Arkanoid]    Script Date: 17/11/2020 17:46:00 ******/
CREATE DATABASE [Arkanoid]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Arkanoid', FILENAME = N'D:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Arkanoid.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Arkanoid_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Arkanoid_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Arkanoid] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Arkanoid].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Arkanoid] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Arkanoid] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Arkanoid] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Arkanoid] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Arkanoid] SET ARITHABORT OFF 
GO
ALTER DATABASE [Arkanoid] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Arkanoid] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Arkanoid] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Arkanoid] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Arkanoid] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Arkanoid] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Arkanoid] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Arkanoid] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Arkanoid] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Arkanoid] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Arkanoid] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Arkanoid] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Arkanoid] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Arkanoid] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Arkanoid] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Arkanoid] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Arkanoid] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Arkanoid] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Arkanoid] SET  MULTI_USER 
GO
ALTER DATABASE [Arkanoid] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Arkanoid] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Arkanoid] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Arkanoid] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Arkanoid] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Arkanoid] SET QUERY_STORE = OFF
GO
USE [Arkanoid]
GO
/****** Object:  Table [dbo].[Estadisticas]    Script Date: 17/11/2020 17:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estadisticas](
	[Id] [int] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Tiempo_Jugado] [bigint] NOT NULL,
	[Partidas_Ganadas] [int] NOT NULL,
	[Partidas_Perdidas] [int] NOT NULL,
	[Promedio_Partidas] [int] NOT NULL,
 CONSTRAINT [PK_Estadisticas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 17/11/2020 17:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Id] [int] NOT NULL,
	[Id_Estadisticas] [int] NOT NULL,
	[Nombre_Usuario] [nvarchar](50) NOT NULL,
	[Contraseña] [nvarchar](50) NOT NULL,
	[Fecha_Alta] [datetime] NOT NULL,
	[Fecha_Baja] [datetime] NULL,
	[Activo] [int] NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios_Log]    Script Date: 17/11/2020 17:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios_Log](
	[Id] [int] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Id_Usuario_Log_Sesion] [int] NOT NULL,
	[Id_Usuario_Log_Partida] [int] NULL,
 CONSTRAINT [PK_Usuarios_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios_Log_Partida]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios_Log_Partida](
	[Id] [int] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Inicio] [datetime] NULL,
	[Fecha_Cierre] [datetime] NULL,
 CONSTRAINT [PK_Usuarios_Log_Partida] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios_Log_Sesion]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios_Log_Sesion](
	[Id] [int] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Inicio] [datetime] NOT NULL,
	[Fecha_Cierre] [datetime] NULL,
 CONSTRAINT [PK_Usuarios_Log_Sesion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Estadisticas] ([Id], [Id_Usuario], [Tiempo_Jugado], [Partidas_Ganadas], [Partidas_Perdidas], [Promedio_Partidas]) VALUES (1, 1, 282, 4, 8, 5)
GO
INSERT [dbo].[Estadisticas] ([Id], [Id_Usuario], [Tiempo_Jugado], [Partidas_Ganadas], [Partidas_Perdidas], [Promedio_Partidas]) VALUES (2, 2, 0, 0, 0, 0)
GO
INSERT [dbo].[Usuarios] ([Id], [Id_Estadisticas], [Nombre_Usuario], [Contraseña], [Fecha_Alta], [Fecha_Baja], [Activo]) VALUES (1, 1, N'admin', N'admin', CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL, 1)
GO
INSERT [dbo].[Usuarios] ([Id], [Id_Estadisticas], [Nombre_Usuario], [Contraseña], [Fecha_Alta], [Fecha_Baja], [Activo]) VALUES (2, 2, N'SuperUsuario', N'MiAngel', CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL, 1)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (1, 1, 1, 1)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (2, 1, 2, 2)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (3, 1, 3, 3)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (4, 1, 4, 4)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (5, 1, 5, 5)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (6, 1, 6, 6)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (7, 1, 7, 7)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (8, 1, 8, 8)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (9, 1, 9, 9)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (10, 1, 10, 10)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (11, 1, 11, 11)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (12, 1, 12, 12)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (13, 1, 13, 13)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (14, 1, 14, 14)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (15, 1, 15, 15)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (16, 1, 16, 16)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (17, 1, 17, 17)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (18, 1, 18, 18)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (19, 1, 19, 19)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (20, 1, 20, 20)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (21, 1, 21, 21)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (22, 1, 22, 22)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (23, 1, 23, 23)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (24, 1, 24, 24)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (25, 1, 25, 25)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (26, 1, 26, 26)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (27, 1, 27, 27)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (28, 1, 28, 28)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (29, 1, 29, 29)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (30, 1, 30, 30)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (31, 1, 31, 31)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (32, 1, 32, 32)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (33, 1, 33, 33)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (34, 1, 34, 34)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (35, 1, 35, 35)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (36, 1, 36, 36)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (37, 1, 37, 37)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (38, 1, 38, 38)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (39, 1, 39, 39)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (40, 1, 40, 40)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (41, 1, 41, 41)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (42, 1, 42, 42)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (43, 1, 43, 43)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (44, 1, 44, 44)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (45, 1, 45, 45)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (46, 1, 46, 46)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (47, 1, 47, 47)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (48, 1, 48, 48)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (49, 1, 49, 49)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (50, 1, 50, 50)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (51, 1, 51, 51)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (52, 1, 52, 52)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (53, 1, 53, 53)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (54, 1, 54, 54)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (55, 1, 55, 55)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (56, 1, 56, 56)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (57, 1, 57, 57)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (58, 1, 58, 58)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (59, 1, 59, 59)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (60, 1, 60, 60)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (61, 1, 61, 61)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (62, 1, 62, 62)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (63, 1, 63, 63)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (64, 1, 64, 64)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (65, 1, 65, 65)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (66, 1, 66, 66)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (67, 1, 67, 67)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (68, 1, 68, 68)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (69, 1, 69, 69)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (70, 1, 70, 70)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (71, 1, 71, 71)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (72, 1, 72, 72)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (73, 1, 73, 73)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (74, 1, 74, 74)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (75, 1, 75, 75)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (76, 1, 76, 76)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (77, 1, 77, 77)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (78, 1, 78, 78)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (79, 1, 79, 79)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (80, 1, 80, 80)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (81, 1, 81, 81)
GO
INSERT [dbo].[Usuarios_Log] ([Id], [Id_Usuario], [Id_Usuario_Log_Sesion], [Id_Usuario_Log_Partida]) VALUES (82, 1, 82, 82)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (1, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (2, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (3, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (4, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (5, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (6, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (7, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (8, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (9, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (10, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (11, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (12, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (13, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (14, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (15, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (16, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (17, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (18, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (19, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (20, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (21, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (22, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (23, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (24, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (25, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (26, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (27, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (28, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (29, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (30, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (31, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (32, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (33, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (34, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (35, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (36, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (37, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (38, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (39, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (40, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (41, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (42, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (43, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (44, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (45, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (46, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (47, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (48, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (49, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (50, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (51, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (52, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (53, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (54, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (55, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (56, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (57, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (58, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (59, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (60, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (61, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (62, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (63, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (64, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (65, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (66, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (67, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (68, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (69, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (70, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (71, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (72, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (73, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (74, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (75, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (76, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (77, 1, CAST(N'2020-11-13T00:00:00.000' AS DateTime), CAST(N'2020-11-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (78, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (79, 1, NULL, CAST(N'2020-11-14T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (80, 1, NULL, NULL)
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (81, 1, CAST(N'2020-11-14T15:12:21.130' AS DateTime), CAST(N'2020-11-14T15:12:35.537' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Partida] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (82, 1, NULL, CAST(N'2020-11-17T17:44:24.797' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (1, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (2, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (3, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (4, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (5, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (6, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (7, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (8, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (9, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (10, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (11, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (12, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (13, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (14, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (15, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (16, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (17, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (18, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (19, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (20, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (21, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (22, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (23, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (24, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (25, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (26, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (27, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (28, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (29, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (30, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (31, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (32, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (33, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (34, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (35, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (36, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (37, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (38, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (39, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (40, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (41, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (42, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (43, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (44, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (45, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (46, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (47, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (48, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (49, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (50, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (51, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (52, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (53, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (54, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (55, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (56, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (57, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (58, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (59, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (60, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (61, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (62, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (63, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (64, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (65, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (66, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (67, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (68, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (69, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (70, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (71, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (72, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (73, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (74, 1, CAST(N'2020-11-11T00:00:00.000' AS DateTime), CAST(N'2020-11-11T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (75, 1, CAST(N'2020-11-13T00:00:00.000' AS DateTime), CAST(N'2020-11-13T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (76, 1, CAST(N'2020-11-13T17:50:15.443' AS DateTime), CAST(N'2020-11-13T17:51:12.193' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (77, 1, CAST(N'2020-11-13T17:51:58.537' AS DateTime), CAST(N'2020-11-13T17:55:29.153' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (78, 1, CAST(N'2020-11-13T17:57:09.073' AS DateTime), CAST(N'2020-11-13T17:58:48.483' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (79, 1, CAST(N'2020-11-14T15:05:03.373' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (80, 1, CAST(N'2020-11-14T15:11:01.853' AS DateTime), NULL)
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (81, 1, CAST(N'2020-11-14T15:12:15.937' AS DateTime), CAST(N'2020-11-14T15:12:35.523' AS DateTime))
GO
INSERT [dbo].[Usuarios_Log_Sesion] ([Id], [Id_Usuario], [Fecha_Inicio], [Fecha_Cierre]) VALUES (82, 1, CAST(N'2020-11-17T17:44:22.213' AS DateTime), CAST(N'2020-11-17T17:44:24.793' AS DateTime))
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Estadisticas] FOREIGN KEY([Id_Estadisticas])
REFERENCES [dbo].[Estadisticas] ([Id])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Estadisticas]
GO
ALTER TABLE [dbo].[Usuarios_Log]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Log_Usuarios] FOREIGN KEY([Id_Usuario])
REFERENCES [dbo].[Usuarios] ([Id])
GO
ALTER TABLE [dbo].[Usuarios_Log] CHECK CONSTRAINT [FK_Usuarios_Log_Usuarios]
GO
ALTER TABLE [dbo].[Usuarios_Log]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Log_Usuarios_Log_Partida] FOREIGN KEY([Id_Usuario_Log_Partida])
REFERENCES [dbo].[Usuarios_Log_Partida] ([Id])
GO
ALTER TABLE [dbo].[Usuarios_Log] CHECK CONSTRAINT [FK_Usuarios_Log_Usuarios_Log_Partida]
GO
ALTER TABLE [dbo].[Usuarios_Log]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Log_Usuarios_Log_Sesion] FOREIGN KEY([Id_Usuario_Log_Sesion])
REFERENCES [dbo].[Usuarios_Log_Sesion] ([Id])
GO
ALTER TABLE [dbo].[Usuarios_Log] CHECK CONSTRAINT [FK_Usuarios_Log_Usuarios_Log_Sesion]
GO
/****** Object:  StoredProcedure [dbo].[Estadisticas_Add]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Estadisticas_Add] @id int, @id_cliente int
as
begin

Insert Into Estadisticas(Id, Id_Usuario, Tiempo_Jugado, Partidas_Ganadas, Partidas_Perdidas, Promedio_Partidas) Values (@id, @id_cliente,0,0,0,0)
end

GO
/****** Object:  StoredProcedure [dbo].[Estadisticas_Mod]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Estadisticas_Mod] 
			@id_usuario int = 1, 
			@tiempo_jugado bigint = 120,
			@partidas_ganadas int = 1, 
			@partidas_perdidas int = 2
as
begin

Update Estadisticas set Tiempo_Jugado = Tiempo_Jugado + @tiempo_jugado, Partidas_Ganadas = @partidas_ganadas + Partidas_Ganadas, Partidas_Perdidas = @partidas_perdidas + Partidas_Perdidas,
						Promedio_Partidas = (Partidas_Ganadas + Partidas_Perdidas) / 2
where Id_Usuario = @id_usuario 

end

GO
/****** Object:  StoredProcedure [dbo].[LeerEstadisticas]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[LeerEstadisticas]
		@id_usuario varchar(50) = 1
as
begin

select * from Estadisticas where Id_Usuario = @id_usuario

end

GO
/****** Object:  StoredProcedure [dbo].[LeerUsuario]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[LeerUsuario] @nombre_usuario varchar(50), @contraseña varchar(50)
as
begin

SELECT Nombre_Usuario, Contraseña
FROM Usuarios 
where Nombre_Usuario = @nombre_usuario and Contraseña = @contraseña

end

GO
/****** Object:  StoredProcedure [dbo].[LeerUsuarioCompleto]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[LeerUsuarioCompleto]
		@nombre_usuario varchar(50) = 'Admin'
as
begin

select * from Usuarios where Nombre_Usuario = @nombre_usuario

end

GO
/****** Object:  StoredProcedure [dbo].[Registro_Usuario_Log_Add]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Registro_Usuario_Log_Add]
		@nombre_usuario varchar(50) = 'Admin'
as
begin

declare @id int
set @id = ISNULL((Select MAX(Id)From Usuarios_Log),0)+1

declare @id_usuario int 
set @id_usuario = (select Id from Usuarios where Nombre_Usuario = @nombre_usuario)

declare @id_usuario_log_partida int
set @id_usuario_log_partida = ISNULL((Select MAX(Id)From Usuarios_Log_Partida),0)+1

declare @id_usuario_log_sesion int
set @id_usuario_log_sesion = ISNULL((Select MAX(Id)From Usuarios_Log_Sesion),0)+1

Insert Into Usuarios_Log_Partida (Id, Id_Usuario, Fecha_Inicio, Fecha_Cierre) values (@id_usuario_log_partida, @id_usuario, null, null)

Insert Into Usuarios_Log_Sesion(Id, Id_Usuario, Fecha_Inicio, Fecha_Cierre) values (@id_usuario_log_sesion, @id_usuario, GETDATE(), null)

Insert Into Usuarios_Log (Id, Id_Usuario, Id_Usuario_Log_Partida, Id_Usuario_Log_Sesion) Values (@id, @id_usuario, @id_usuario_log_partida, @id_usuario_log_sesion)
end

GO
/****** Object:  StoredProcedure [dbo].[Registro_Usuario_Log_Partida_Mod]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Registro_Usuario_Log_Partida_Mod]
@nombre_usuario varchar(50) = 'admin', 
@fecha_inicio datetime = null, 
@fecha_cierre datetime = null
as
begin

	declare @id_usuario int
	set @id_usuario = (select Id from Usuarios where Nombre_Usuario = @nombre_usuario)

	declare @id int
	set @id = (select Max(id) from Usuarios_Log_Partida where Id_Usuario = @id_usuario)
	print @fecha_inicio
	if @fecha_inicio is null
		begin
		print 'hola'
			Update Usuarios_Log_Partida set Fecha_Cierre = @fecha_cierre Where Id = @id
		
	end

	if @fecha_cierre is null
		begin
			Update Usuarios_Log_Partida set Fecha_Inicio = @fecha_inicio Where Id = @id
		
	end

end

GO
/****** Object:  StoredProcedure [dbo].[Registro_Usuario_Log_Sesion_Mod]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Registro_Usuario_Log_Sesion_Mod]
		@nombre_usuario varchar(50) = 'Admin'
as
begin

declare @id_usuario int
set @id_usuario = (select Id from Usuarios where Nombre_Usuario = @nombre_usuario)

update Usuarios_Log_Sesion set Fecha_Cierre = GETDATE() where id = (select Max(id) from Usuarios_Log_Sesion where Id_Usuario = @id_usuario)

end

GO
/****** Object:  StoredProcedure [dbo].[Usuario_Add]    Script Date: 17/11/2020 17:46:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Usuario_Add] 
		@nombre_usuario varchar(50) = 'JorgeDestroyer', 
		@contraseña varchar(50) = 'Admin', 
		@fecha_alta date = '20201111'
as
begin

declare @id int
set @id = ISNULL((Select MAX(Id)From Usuarios),0)+1

declare @id_estadisticas int
set @id_estadisticas = ISNULL((Select MAX(Id)From Estadisticas),0)+1

exec [dbo].[Estadisticas_Add] @id_estadisticas, @id

Insert Into Usuarios(Id,Id_Estadisticas,Nombre_Usuario, Contraseña, Fecha_Alta, Activo) Values (@id, @id_estadisticas, @nombre_usuario, @contraseña, @fecha_alta, 1)

end
GO
USE [master]
GO
ALTER DATABASE [Arkanoid] SET  READ_WRITE 
GO

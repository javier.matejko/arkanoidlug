﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Motor
{
    public class Estadisticas
    {
        AccesoDatos.MP_Estadisticas mp_Estadisticas = new AccesoDatos.MP_Estadisticas();

        public DataTable LeerEstadisticas(Entidades.Usuario usuario)
        {
            DataTable tabla = mp_Estadisticas.LeerEstadisticas(usuario);
            return tabla;
        }
    }
}

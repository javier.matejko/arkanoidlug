﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace Motor
{
    public class Juego
    {
        Pelota pelota;
        Jugador jugador;
        List<Bloque> bloques;
        private float tiempo;

        public float Tiempo
        {
            get { return tiempo; }
        }
        public Jugador Jugador
        {
            get { return jugador; }
        }
        public Pelota Pelota
        {
            get { return pelota; }
        }
        public void Inicializar()
        {
            InicializarJugador();
            InicializarPelota();
            //InicializarBloques();
        }

        private void InicializarJugador()
        {
            if (jugador == null) jugador = new Jugador();

            jugador.Puntaje = 0;
            jugador.Vidas = 3;
            jugador.Velocidad = 15;
        }

        private void InicializarPelota()
        {
            if (pelota == null) pelota = new Pelota();

        }

        //private void InicializarBloques(List<Bloque> _bloques)
        //{
        //    bloques = _bloques;
        //}

        public int ActualizarPuntaje()
        {
            return jugador.Puntaje;
        }

        public void MoverIzquierda()
        {
            if(jugador.X > -1)
            {
                jugador.X = -jugador.Velocidad;
            }            
        }

        public void MoverDerecha(int ancho)
        {
            if (jugador.X < ancho)
            {
                jugador.X = jugador.Velocidad;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motor
{
    public class Usuario
    {
        AccesoDatos.MP_Usuario mp_Usuario = new AccesoDatos.MP_Usuario();

        public void Guardar(Entidades.Usuario usuario)
        {
            if(usuario.Id == 0)
            {
                mp_Usuario.Insertar(usuario);
            }
            else
            {
                //error
            }
        }

        public int Ingresar(Entidades.Usuario usuario)
        {
            int registros = mp_Usuario.Leer(usuario);

            return registros;
        }

        public DataTable LeerUsuario(Entidades.Usuario usuario)
        {
            DataTable tabla = mp_Usuario.LeerUsuario(usuario);
            return tabla;
        }

        public void RegistroSesion(Entidades.Usuario usuario)
        {
            mp_Usuario.RegistrarSesion(usuario);
        }

        public void CerrarSesion(Entidades.Usuario usuario)
        {
            mp_Usuario.CerrarSesion(usuario);
        }

        public void RegistrarEstadisticas(Entidades.Usuario usuario, Int64 tiempoJugado, bool gano)
        {
            mp_Usuario.RegistrarEstadisticas(usuario, tiempoJugado, gano);
        }

        public void RegistrarInicioSesionPartida(Entidades.Usuario usuario)
        {
            mp_Usuario.RegistrarInicioSesionPartida(usuario);
        }
        public void RegistrarCierreSesionPartida(Entidades.Usuario usuario)
        {
            mp_Usuario.RegistrarCierreSesionPartida(usuario);
        }
    }
}
